# README. Spring User Service #

#1. Описание.

Реализован REST сервис, отвечающий требованиям:

* Хранятся данные пользователя (Имя(*firstName*), фамилия(*lastName*), дата рождения(*date*), email(*email*) и пароль(*password*)). Для хранения используется *mongoDB*. Имя базы данных: ***sievmi_user_service***

* Пароль пользователя хранится в безопасной форме. Используется *org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder*

* Предоставляемый функционал: добавление(*POST*), удаление(*DELETE*), изменение(*PUT*) и поиск пользователя по email. 


***НЕ РЕАЛИЗОВАНО***, т.к. это не требовалось, но при необходимости можно доделать:

* Проверка валидности данных полльлзователя

* Проверка существования пользователя с данным email при добавлении



#2. Инструкция по сборке и запуску.

$ git clone https://sievmi@bitbucket.org/sievmi/springuserservice.git

$ cd springuserservice/

$ mvn clean package

$ mvn spring-boot:run

#3. Инструкция по использованию. Примеры.

Будем использовать ***curl*** для взаимодействия с приложением.

###3.1 Запрос для получения информации обо всех пользователях.

$ curl http://localhost:8080/users

###3.2 Добавление нового пользователя.

$ curl -i -X POST -H "Content-Type:application/json" -d "{  \"firstName\" : \"Evgeny\",  \"lastName\" : \"Sidorov\", \"email\" : \"sievmi@mail.ru\", \"date\" : \"1997-06-27\", \"password\" : \"123\" }" http://localhost:8080/users


###3.3  Получение информации о пользователе по его ID 

$ curl http://localhost:8080/users/{ID}

###3.4 Поиск пользователя по email.

$ curl http://localhost:8080/users/search/findByEmail?email=sievmi@mail.ru

###3.5 Удаление пользователя по его ID.

$ curl -X DELETE http://localhost:8080/users/{ID}